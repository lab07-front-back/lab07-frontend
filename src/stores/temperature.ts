import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import http from '@/services/http'
import { useLoadingStore } from './loading'
import temperatureServices from '@/services/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()


  
async function callConvert() {
  //console.log('callConvert:store')
  //result.value = convert(celsius.value)
  loadingStore.doLoad()
  try {
    result.value = await temperatureServices.convert(celsius.value)
  } catch (e) {
    console.log(e)
  }
  loadingStore.finishLoad()
  //console.log('fin callConvert:store')
}
  return {valid,celsius,result,callConvert}
})
