import http from "./http"

type ReturData = {
    celsius: number
    fahrenheit: number
  }

async function convert(celsius: number):Promise<number>{
    console.log('callConvert:services')
    console.log(`/temperature/convert/${celsius}`)
    const res = await http.post(`/temperature/convert`, {
      celsius: celsius
    })
    const convertReturData = res.data as ReturData
    console.log('fin callConvert:services')
    return convertReturData.fahrenheit
}

export default {  convert }